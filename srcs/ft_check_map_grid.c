/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_map_grid.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 05:11:45 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/16 11:13:42 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_check_map_grid_sprite(t_app *app, int x, int y)
{
	t_sprite	*sprite;

	if (!ft_check_map_indexes(app, x, y))
		return ;
	if (app->map.map_grid[y][x] == '2')
	{
		if ((sprite = app->map.sprite_list))
			while (sprite->next)
				sprite = sprite->next;
		ft_malloc_string(app, (char **)(sprite ? &sprite->next : &sprite),
				sizeof(t_sprite));
		if (sprite->pos.y)
			sprite = sprite->next;
		*sprite = (t_sprite) {
			.pos = (t_vector) {
				.x = x + 0.5,
				.y = y + 0.5
			}
		};
		if (!app->map.sprite_list)
			app->map.sprite_list = sprite;
	}
}

void	ft_check_map_grid_player_start(t_app *app, int x, int y)
{
	if (!ft_check_map_indexes(app, x, y))
		return ;
	ft_check_multi_player(app, x, y);
	if (app->map.map_grid[y][x] == 'S')
	{
		app->map.player_start.pos = (t_vector){x, y};
		app->map.player_start.dir = (t_vector){0, 1};
	}
	else if (app->map.map_grid[y][x] == 'W')
	{
		app->map.player_start.pos = (t_vector){x, y};
		app->map.player_start.dir = (t_vector){-1, 0};
	}
	else if (app->map.map_grid[y][x] == 'E')
	{
		app->map.player_start.pos = (t_vector){x, y};
		app->map.player_start.dir = (t_vector){1, 0};
	}
	else if (app->map.map_grid[y][x] == 'N')
	{
		app->map.player_start.pos = (t_vector){x, y};
		app->map.player_start.dir = (t_vector){0, -1};
	}
	ft_check_map_grid_sprite(app, x, y);
	ft_check_map_grid_validation(app, x, y);
}

void	ft_set_next_pos(t_vector *painting_pos, t_int_vector current_pos,
						char sep)
{
	if (painting_pos->x || painting_pos->y)
		ft_set_next_pos(&painting_pos[1], current_pos, sep);
	else if (sep)
		painting_pos[0] = (t_vector) {
			.x = 0x7fffffffL,
			.y = 0x7fffffffL
		};
	else
		painting_pos[0] = (t_vector) {
			.x = current_pos.x,
			.y = current_pos.y
		};
}

void	ft_set_paint_on_wall(t_app *app)
{
	t_int_vector	pos;

	pos = (t_int_vector) {
		.x = -1,
		.y = -1
	};
	ft_malloc_string(app, (char **)&app->map.painting_pos, sizeof(t_vector)
	* (app->map.map_max_height * app->map.map_max_width));
	ft_set_paint_west_wall(app, &pos);
	ft_set_next_pos(app->map.painting_pos, pos, 1);
	ft_set_paint_east_wall(app, &pos);
	ft_set_next_pos(app->map.painting_pos, pos, 1);
	ft_set_paint_north_wall(app, &pos);
	ft_set_next_pos(app->map.painting_pos, pos, 1);
	ft_set_paint_south_wall(app, &pos);
	ft_set_next_pos(app->map.painting_pos, pos, 1);
}

void	ft_check_map_grid(t_app *app)
{
	int		x;
	int		y;
	int		len;

	y = -1;
	while (app->map.map_grid[app->map.map_max_height])
		app->map.map_max_height++;
	while (++y < app->map.map_max_height)
	{
		x = -1;
		if ((len = ft_strlen(app->map.map_grid[y])) > app->map.map_max_width)
			app->map.map_max_width = len;
		while (y && ++x < len)
			ft_check_map_grid_player_start(app, x, y);
	}
	if (!app->map.player_start.pos.x && !app->map.player_start.pos.y)
		ft_init_error(app, ERROR_NO_PLAYER);
	ft_set_paint_on_wall(app);
	ft_right_hand_check(app);
	free(app->map.painting_pos);
	app->map.painting_pos = NULL;
}
