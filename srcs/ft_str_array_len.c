/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_len.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/23 02:54:05 by tbrouill          #+#    #+#             */
/*   Updated: 2020/06/23 15:31:00 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		ft_atoi2(t_app *app, const char *str)
{
	int i;
	int	start;
	int result;

	result = 0;
	i = 0;
	while (str[i] && (str[i] == '\n' || str[i] == '\t' || str[i] == '\r'
	|| str[i] == '\v' || str[i] == '\f' || str[i] == ' '))
		i++;
	start = i;
	i = (str[i] == '-' || str[i] == '+') ? i + 1 : i;
	while (str[i] > 47 && str[i] < 58)
	{
		result = result * 10 + (str[i] - '0');
		i++;
	}
	if (str[i]
	&& ((str[i] > 32 && str[i] < 48) || (str[i] > 57 && str[i] < 127)))
		ft_init_error(app, "Unexpected character.");
	return ((str[start] == '-') ? result * -1 : result);
}

size_t	ft_str_array_len(char **str_array)
{
	size_t	len;

	len = -1;
	while (str_array[++len])
		;
	return (len);
}
