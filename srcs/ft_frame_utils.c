/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_frame_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: root <root@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 17:53:10 by root              #+#    #+#             */
/*   Updated: 2020/07/04 00:47:08 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_set_draw_values(t_app *app, const t_sprite *sprite, t_img *img,
		t_int_vector *sprite_size)
{
	img->height = sprite_size->y;
	img->width = sprite_size->x;
	ft_init_image(app, img);
	img->draw_start.y = -sprite_size->y / 2 + app->win_height / 2;
	if (img->draw_start.y < 0)
		img->draw_start.y = 0;
	img->draw_end.y = sprite_size->y / 2 + app->win_height / 2;
	if (img->draw_end.y >= app->win_height)
		img->draw_end.y = app->win_height - 1;
	img->draw_start.x = -sprite_size->x / 2 + sprite->screen_x;
	if (img->draw_start.x < 0)
		img->draw_start.x = 0;
	img->draw_end.x = sprite_size->x / 2 + sprite->screen_x;
	if (img->draw_end.x >= app->win_width)
		img->draw_end.x = app->win_width - 1;
}

void	get_tex_pos(t_app *app, t_int_vector *sprite_size, int y,
		t_int_vector *texture_pos)
{
	(*texture_pos).y = abs(((y * 256 - app->win_height * 128
			+ (*sprite_size).y * 128) * app->map.sprite_texture.height)
					/ (*sprite_size).y) / 256;
}

void	ft_put_sprite_to_frame(t_app *app, const t_sprite *sprite,
		t_int_vector *sprite_size, t_vector *transform)
{
	int				stripe;
	int				y;
	t_color			color;
	t_int_vector	texture_pos;

	stripe = sprite->img.draw_start.x - 1;
	while (++stripe < sprite->img.draw_end.x)
	{
		texture_pos.x = (stripe - (-(*sprite_size).x / 2
				+ sprite->screen_x)) * app->map.sprite_texture.width
						/ (*sprite_size).x;
		if ((*transform).y > 0 && sprite > 0 && stripe < app->win_width
			&& (*transform).y <= app->dist_buffer[stripe])
		{
			y = sprite->img.draw_start.y - 1;
			while (++y < sprite->img.draw_end.y)
			{
				get_tex_pos(app, sprite_size, y, &texture_pos);
				color = *ft_get_pixel_addr(&app->map.sprite_texture,
						texture_pos.x, texture_pos.y);
				if (color & 0x00FFFFFF)
					ft_set_pixel(&app->frame, stripe, y, color);
			}
		}
	}
}

void	ft_put_bmp_header_to_file(int fd, t_img *img)
{
	t_bmp_header	bmp_h;
	t_dib_header	dib_h;

	bmp_h = (t_bmp_header) {
			.magic = 0x4d42,
			.size = 0,
			.img_offset = sizeof(bmp_h) + sizeof(dib_h)
	};
	dib_h = (t_dib_header) {
			.header_size = 40,
			.img_width = img->width,
			.img_height = img->height,
			.color_planes_nbr = 1,
			.size_of_pixel = 32,
			.horizontal_res = 96,
			.vertical_res = 96,
			.compression_method = 0
	};
	write(fd, &bmp_h, sizeof(t_bmp_header));
	write(fd, &dib_h, sizeof(t_dib_header));
}

void	ft_put_image_to_file(t_img *img, int fd)
{
	int		x;
	int		y;
	t_color	*pixel;

	y = img->height + 1;
	ft_put_bmp_header_to_file(fd, img);
	while (--y > 0)
	{
		x = -1;
		while (++x < img->width)
		{
			pixel = ft_get_pixel_addr(img, x, y);
			write(fd, pixel, sizeof(*pixel));
		}
	}
	close(fd);
}
