/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_multi_iteration.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/17 23:45:40 by root              #+#    #+#             */
/*   Updated: 2020/06/17 23:50:43 by root             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_do_check_iter(t_app *app, char *check, const char *error)
{
	if (*check)
		ft_init_error(app, error);
	*check = 1;
}

char	ft_check_multi_iteration(t_app *app, char param)
{
	if (param == 'N')
		ft_do_check_iter(app, &app->map.is_parsed.north, ERROR_M_NO);
	else if (param == 'S')
		ft_do_check_iter(app, &app->map.is_parsed.south, ERROR_M_SO);
	else if (param == 'E')
		ft_do_check_iter(app, &app->map.is_parsed.east, ERROR_M_EA);
	else if (param == 'W')
		ft_do_check_iter(app, &app->map.is_parsed.west, ERROR_M_WE);
	else if (param == 's')
		ft_do_check_iter(app, &app->map.is_parsed.sprite, ERROR_M_S);
	else if (param == 'M')
		ft_do_check_iter(app, &app->map.is_parsed.map, ERROR_M_M);
	return (0);
}
