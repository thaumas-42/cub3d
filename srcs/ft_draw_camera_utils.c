/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_camera_utils.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: root <root@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 16:53:52 by root              #+#    #+#             */
/*   Updated: 2020/07/17 17:49:51 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_check_hit(t_app *app, t_ray *ray)
{
	if (!ft_check_map_indexes(app, ray->map.x, ray->map.y)
	|| app->map.map_grid[ray->map.y][ray->map.x] == '1')
		ray->hit = 1;
}

int		ft_check_map_indexes(t_app *app, t_uint x, t_uint y)
{
	if (y < 0 || y > (t_uint)app->map.map_max_height
	|| x < 0 || x > ft_strlen(app->map.map_grid[y]))
		return (0);
	return (1);
}

void	ft_dda(t_app *app, t_ray *ray, t_vector *side_dist)
{
	while (!ray->hit)
	{
		if (side_dist->x < side_dist->y)
		{
			side_dist->x += ray->delta_dist.x;
			ray->map.x += ray->step.x;
			if (ray->dir.x > 0)
				ray->hit_side = 0;
			else
				ray->hit_side = 1;
		}
		else
		{
			side_dist->y += ray->delta_dist.y;
			ray->map.y += ray->step.y;
			if (ray->dir.y > 0)
				ray->hit_side = 2;
			else
				ray->hit_side = 3;
		}
		ft_check_hit(app, ray);
	}
}

void	ft_get_texture_from_face(t_app *app, t_ray *ray)
{
	if (ray->hit_side == 1)
		ray->texture = &app->map.w_texture;
	else if (!ray->hit_side)
		ray->texture = &app->map.e_texture;
	else if (ray->hit_side == 3)
		ray->texture = &app->map.n_texture;
	else
		ray->texture = &app->map.s_texture;
}

void	ft_set_line_init(t_app *app, t_ray *ray, int *line_start, int *line_end)
{
	*line_start = -ray->line_height / 2 + (int)app->win_height / 2;
	*line_start = *line_start < 0 ? 0 : *line_start;
	*line_end = ray->line_height / 2 + (int)app->win_height / 2;
	*line_end = (*line_end) < 0 ? 0 : *line_end;
	ft_get_texture_from_face(app, ray);
	if (ray->hit_side < 2)
		ray->wall_pos_x = ray->pos.y + ray->wall_dist * ray->dir.y;
	else
		ray->wall_pos_x = ray->pos.x + ray->wall_dist * ray->dir.x;
	ray->wall_pos_x -= floor(ray->wall_pos_x);
}
