/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_map_grid.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 03:15:32 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/17 16:53:12 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_malloc_string(t_app *app, char **str, int size)
{
	if (!(*str = malloc(sizeof(char *) * size)))
		ft_init_error(app, ERROR_MEM);
	ft_bzero(*str, sizeof(char *) * size);
}

void	ft_get_map_grid_from_file(t_app *app, char **str, const int map_fd)
{
	char	*tmp;
	char	*buff;

	tmp = *str;
	while (get_next_line(map_fd, str) > 0)
	{
		buff = tmp;
		tmp = ft_strjoin(tmp, "\n");
		free(buff);
		buff = tmp;
		tmp = ft_strjoin(tmp, *str);
		free(buff);
		free(*str);
	}
	buff = tmp;
	tmp = ft_strjoin(tmp, "\n");
	free(buff);
	buff = tmp;
	tmp = ft_strjoin(tmp, *str);
	free(buff);
	free(*str);
	*str = NULL;
	app->map.map_grid = ft_split(tmp, '\n');
	free(tmp);
}

void	ft_get_map_grid(t_app *app, char **str, int map_fd)
{
	int		i;

	i = 0;
	app->map.is_parsed.map = 1;
	while ((*str)[i] && (((*str)[i] >= '0' && (*str)[i] <= '2')
	|| (*str)[i] == 'N' || (*str)[i] == 'S' || (*str)[i] == 'W'
	|| (*str)[i] == 'E' || (*str)[i] == ' '))
		i++;
	if (!(*str)[i])
	{
		ft_get_map_grid_from_file(app, str, map_fd);
		ft_check_map_grid(app);
	}
	else if (i == 0)
		return ;
	else
		ft_init_error(app, ERROR_MAP_BAD_CHAR);
}
