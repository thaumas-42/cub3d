/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right_hand_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/28 16:18:22 by thaumas           #+#    #+#             */
/*   Updated: 2020/07/17 17:51:27 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_turn_right_hand(t_app *app, char x)
{
	app->right_hand.dir = (app->right_hand.dir + 1 * x) % 4;
	if (ft_seek_paint(app->map.painting_pos, app->right_hand.pos,
			app->right_hand.dir))
		ft_init_error(app, ERROR_MAP_OPEN);
}

void	ft_check_start_pos(t_app *app, t_int_vector start_pos, int *x)
{
	*x = -1;
	app->right_hand.pos = start_pos;
	while (app->right_hand.dir || !++(*x))
	{
		ft_check_wall_right_hand(app);
		ft_turn_right_hand(app, 1);
	}
	*x = -1;
}

void	ft_move_right_hand(t_app *app)
{
	if (app->right_hand.dir == NORTH)
		app->right_hand.pos.y--;
	else if (app->right_hand.dir == SOUTH)
		app->right_hand.pos.y++;
	else if (app->right_hand.dir == EAST)
		app->right_hand.pos.x++;
	else if (app->right_hand.dir == WEST)
		app->right_hand.pos.x--;
	if (!ft_check_map_indexes(app, app->right_hand.pos.x,
		app->right_hand.pos.y))
		ft_init_error(app, ERROR_MAP_OPEN);
	if (app->map.map_grid[app->right_hand.pos.y][app->right_hand.pos.x] == ' ')
		ft_init_error(app, ERROR_MAP_OPEN);
	if (app->map.map_grid[app->right_hand.pos.y][app->right_hand.pos.x] == '1'
		&& !app->right_hand.start_pos.x)
	{
		ft_turn_right_hand(app, 3);
		ft_move_right_hand(app);
	}
	if (ft_seek_paint(app->map.painting_pos, app->right_hand.pos,
					app->right_hand.dir))
		ft_init_error(app, ERROR_MAP_OPEN);
}

void	ft_move_right_hand_angle(t_app *app)
{
	ft_turn_right_hand(app, 1);
	ft_move_right_hand(app);
	if (ft_seek_paint(app->map.painting_pos, app->right_hand.pos,
					app->right_hand.dir))
		ft_init_error(app, ERROR_MAP_OPEN);
}

char	ft_check_wall_right_hand(t_app *app)
{
	if (ft_seek_paint(app->map.painting_pos, app->right_hand.pos,
					app->right_hand.dir)
		|| app->right_hand.pos.x >= app->map.map_max_width - 1
		|| app->right_hand.pos.y >= app->map.map_max_height - 1
		|| !app->right_hand.pos.y || !app->right_hand.pos.x)
		ft_init_error(app, ERROR_MAP_OPEN);
	if (app->right_hand.dir == NORTH)
		return ((char)(app->map.map_grid[app->right_hand.pos.y]
		[app->right_hand.pos.x + 1] == '1'));
	else if (app->right_hand.dir == EAST)
		return ((char)(app->map.map_grid[app->right_hand.pos.y + 1]
		[app->right_hand.pos.x] == '1'));
	else if (app->right_hand.dir == WEST)
		return ((char)(app->map.map_grid[app->right_hand.pos.y - 1]
		[app->right_hand.pos.x] == '1'));
	else
		return ((char)(app->map.map_grid[app->right_hand.pos.y]
		[app->right_hand.pos.x - 1] == '1'));
}
