/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_texture.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 01:13:04 by tbrouill          #+#    #+#             */
/*   Updated: 2020/05/13 15:31:52 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	get_texture(t_app *app, char *str, t_img *map_texture, char *err)
{
	char	**tmp;

	tmp = ft_split(str, ' ');
	map_texture->data = mlx_xpm_file_to_image(app->mlx, tmp[1],
			&map_texture->width, &map_texture->height);
	free(tmp[0]);
	free(tmp[1]);
	free(tmp);
	if (!map_texture->data)
	{
		app->map.init = 0;
		ft_init_error(app, err);
	}
	map_texture->addr = mlx_get_data_addr(map_texture->data,
			&map_texture->bits_per_pixel, &map_texture->line_length,
			&map_texture->endian);
}
