/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keystroke.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 00:40:46 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/16 16:05:57 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_reset_norme_length(t_app *app)
{
	app->player.plane.x /= sqrt(app->player.plane.x * app->player.plane.x
			+ app->player.plane.y * app->player.plane.y);
	app->player.plane.y /= sqrt(app->player.plane.x * app->player.plane.x
			+ app->player.plane.y * app->player.plane.y);
	app->player.dir.x /= sqrt(app->player.dir.x * app->player.dir.x
			+ app->player.dir.y * app->player.dir.y);
	app->player.dir.y /= sqrt(app->player.dir.x * app->player.dir.x
			+ app->player.dir.y * app->player.dir.y);
}

void	ft_keystroke_do_move_up(t_app *app)
{
	if (app->key_mem.mv_up)
	{
		if (app->map.map_grid[(int)app->player.pos.y]
		[(int)(app->player.pos.x + SPEED * app->player.dir.x)] != '1')
			app->player.pos.x += SPEED * app->player.dir.x;
		if (app->map.map_grid[(int)(app->player.pos.y
		+ SPEED * app->player.dir.y)][(int)app->player.pos.x] != '1')
			app->player.pos.y += SPEED * app->player.dir.y;
	}
}

int		ft_keystroke(t_app *app)
{
	ft_keystroke_do_move_up(app);
	ft_keystroke_do_move_down(app);
	ft_keystroke_do_move_left(app);
	ft_keystroke_do_move_right(app);
	ft_keystroke_do_turn_left(app);
	ft_keystroke_do_turn_right(app);
	ft_reset_norme_length(app);
	return (0);
}

int		ft_keystroke_press(int keycode, t_app *app)
{
	if (keycode == QUIT)
		ft_quit(app);
	else if (keycode == MOVE_FORWARD)
		app->key_mem.mv_up = 1;
	else if (keycode == MOVE_BACKWARD)
		app->key_mem.mv_down = 1;
	else if (keycode == MOVE_LEFT)
		app->key_mem.mv_left = 1;
	else if (keycode == MOVE_RIGHT)
		app->key_mem.mv_right = 1;
	else if (keycode == TURN_LEFT && !(app->key_mem.left || app->key_mem.right))
		app->key_mem.left = 1;
	else if (keycode == TURN_RIGHT
	&& !(app->key_mem.left || app->key_mem.right))
		app->key_mem.right = 1;
	else if (keycode == TOGGLE_MINIMAP)
		app->player.show = (app->player.show + 1) % 3;
	return (0);
}

int		ft_keystroke_release(int keycode, t_app *app)
{
	if (keycode == MOVE_FORWARD)
		app->key_mem.mv_up = 0;
	else if (keycode == MOVE_BACKWARD)
		app->key_mem.mv_down = 0;
	else if (keycode == MOVE_LEFT)
		app->key_mem.mv_left = 0;
	else if (keycode == MOVE_RIGHT)
		app->key_mem.mv_right = 0;
	else if (keycode == TURN_LEFT)
		app->key_mem.left = 0;
	else if (keycode == TURN_RIGHT)
		app->key_mem.right = 0;
	return (0);
}
