/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_images.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/28 17:57:14 by tbrouill          #+#    #+#             */
/*   Updated: 2020/05/14 18:58:56 by root             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_put_camera_frame(const t_app *app)
{
	mlx_put_image_to_window(app->mlx, app->win, app->frame.data, 0, 0);
	mlx_destroy_image(app->mlx, app->frame.data);
}
