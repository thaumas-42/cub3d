/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_frame.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 00:36:24 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/14 16:37:00 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_do_sprite_calculation(t_app *app, t_sprite *sprite)
{
	t_img			img;
	double			inv;
	t_int_vector	sprite_size;
	t_vector		transform;

	inv = 1.0 / (app->player.plane.x * app->player.dir.y
			- app->player.dir.x * app->player.plane.y);
	sprite->v_dist.x = sprite->pos.x - app->player.pos.x;
	sprite->v_dist.y = sprite->pos.y - app->player.pos.y;
	transform = (t_vector) {
		.x = inv * (app->player.dir.y * sprite->v_dist.x
				- app->player.dir.x * sprite->v_dist.y),
		.y = inv * (-app->player.plane.y * sprite->v_dist.x
					+ app->player.plane.x * sprite->v_dist.y)
	};
	sprite->screen_x = (int)((app->win_width / 2.0)
			* (1 + transform.x / transform.y));
	sprite_size.y = abs((int)((app->win_height / 2.0) / transform.y));
	sprite_size.x = abs((int)((app->win_height / 2.0) / transform.y))
			* (app->map.sprite_texture.width / app->map.sprite_texture.height);
	ft_set_draw_values(app, sprite, &img, &sprite_size);
	sprite->img = img;
	ft_put_sprite_to_frame(app, sprite, &sprite_size, &transform);
	mlx_destroy_image(app->mlx, img.data);
}

double	ft_pythagore(t_vector pos1, t_vector pos2)
{
	return (sqrt((pos1.x - pos2.x) * (pos1.x - pos2.x) + (pos1.y - pos2.y)
														* (pos1.y - pos2.y)));
}

void	ft_draw_sprite(t_app *app)
{
	t_sprite		*sprite;
	int				len;

	if (app->map.sprite_list)
	{
		sprite = app->map.sprite_list;
		len = 1;
		while (sprite->next)
		{
			sprite->dist = ft_pythagore(app->player.pos, sprite->pos);
			len++;
			sprite = sprite->next;
		}
		sprite->dist = ft_pythagore(app->player.pos, sprite->pos);
		sprite = app->map.sprite_list;
		ft_bubbleshort(app->map.sprite_list, len);
		while (sprite->next)
		{
			ft_do_sprite_calculation(app, sprite);
			sprite = sprite->next;
		}
		ft_do_sprite_calculation(app, sprite);
	}
}

char	ft_is_view_updated(t_app *app)
{
	return ((char)(app->player.plane.x != app->player_old.plane.x
	|| app->player.pos.x != app->player_old.pos.x
	|| app->player.pos.y != app->player_old.pos.y
	|| app->player.show != app->player_old.show));
}

int		ft_frame(t_app *app)
{
	int			fd;

	if (app->save_path)
	{
		fd = open(app->save_path, O_WRONLY | O_CREAT, 0777);
		ft_init_image(app, &app->frame);
		ft_get_camera_frame(app);
		ft_draw_sprite(app);
		ft_put_image_to_file(&app->frame, fd);
		mlx_destroy_image(app->mlx, app->frame.data);
		close(fd);
		ft_quit(app);
	}
	ft_keystroke(app);
	if (ft_is_view_updated(app))
	{
		ft_draw_camera(app);
		ft_draw_sprite(app);
		ft_draw_minimap(app);
		ft_put_camera_frame(app);
		app->player_old = app->player;
	}
	return (0);
}
