/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_pixel_line_utils.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 16:58:13 by root              #+#    #+#             */
/*   Updated: 2020/07/14 18:00:30 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_set_line_set_hitpos_array(t_app *app, const t_ray *ray)
{
	app->ray_hitpos_array[ray->x] = (t_vector) {
			.x = (ray->hit_side == 1) ? ray->map.x + 1 : ray->map.x,
			.y = (ray->hit_side == 3) ? ray->map.y + 1 : ray->map.y
	};
	if (ray->hit_side >= 2)
		app->ray_hitpos_array[ray->x].x += ray->wall_pos_x;
	else
		app->ray_hitpos_array[ray->x].y += ray->wall_pos_x;
}

void	ft_set_line_init_img_pos(t_app *app, t_ray *ray, t_vector *img_pos,
		int line_start)
{
	img_pos->x = (ray->wall_pos_x * (double)ray->texture->width);
	img_pos->x = (ray->hit_side < 2 && ray->dir.x > 0) ?
			(ray->texture->width - img_pos->x - 1) : img_pos->x;
	img_pos->x = (ray->hit_side >= 2 && ray->dir.y < 0) ?
			(ray->texture->width - img_pos->x - 1) : img_pos->x;
	ray->texture_step = (double)ray->texture->height / (double)ray->line_height;
	img_pos->y = (line_start - app->win_height / 2.0 + ray->line_height / 2.0)
			* ray->texture_step;
}
