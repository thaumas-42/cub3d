/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_paint_on_wall_utils.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/13 15:48:08 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/17 17:53:07 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_set_paint_north_wall(t_app *app, t_int_vector *pos)
{
	pos->x = -1;
	while (++pos->x < app->map.map_max_width)
	{
		pos->y = -1;
		while (++pos->y < app->map.map_max_height)
			if (ft_check_map_indexes(app, pos->x, pos->y) &&
			app->map.map_grid[pos->y][pos->x] == '1')
				ft_set_next_pos(app->map.painting_pos, *pos, 0);
	}
}

void	ft_set_paint_west_wall(t_app *app, t_int_vector *pos)
{
	while (++pos->y < app->map.map_max_height)
	{
		pos->x = -1;
		while (++pos->x < app->map.map_max_width)
			if (app->map.map_grid[pos->y][pos->x] == '1')
				ft_set_next_pos(app->map.painting_pos, *pos, 0);
			else if (app->map.map_grid[pos->y][pos->x] == '\0')
				break ;
	}
}

void	ft_set_paint_east_wall(t_app *app, t_int_vector *pos)
{
	while (--pos->y > -1)
	{
		pos->x = (int)ft_strlen(app->map.map_grid[pos->y]) - 1;
		while (--pos->x > -1)
			if (app->map.map_grid[pos->y][pos->x] == '1')
				ft_set_next_pos(app->map.painting_pos, *pos, 0);
	}
}

void	ft_set_paint_south_wall(t_app *app, t_int_vector *pos)
{
	pos->x = (int)ft_strlen(app->map.map_grid[app->map.map_max_height]) - 1;
	while (--pos->x > -1)
	{
		pos->y = app->map.map_max_height;
		if (app->map.map_grid[pos->y][pos->x] == '\0')
			continue ;
		while (--pos->y > -1)
			if (app->map.map_grid[pos->y][pos->x] == '1')
				ft_set_next_pos(app->map.painting_pos, *pos, 0);
		pos->x = (int)ft_strlen(app->map.map_grid[pos->y - 1]) - 1;
	}
}
