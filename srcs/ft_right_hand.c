/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_right_hand.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/29 17:12:40 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/16 12:40:11 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

char	ft_seek_paint(t_vector *painting_list, t_int_vector current_pos,
		t_dir dir)
{
	int		i;
	int		i_max;

	i_max = -1;
	i = 0;
	while (painting_list[++i_max].x != 0x7fffffffL)
		;
	if (dir == WEST || dir == EAST)
		while (painting_list[++i_max].x != 0x7fffffffL)
			;
	if (dir == EAST)
		while (painting_list[++i_max].x != 0x7fffffffL)
			;
	if (dir != NORTH && (i = i_max + 1))
		while (painting_list[++i_max].x != 0x7fffffffL)
			;
	while (!(painting_list[i].x == current_pos.x
	&& painting_list[i].y == current_pos.y) && i < i_max)
		i++;
	if (painting_list[i].x == current_pos.x
	&& painting_list[i].y == current_pos.y)
		return (1);
	return (0);
}

void	ft_right_hand_do_next_move(t_app *app)
{
	if (!ft_check_wall_right_hand(app))
		ft_move_right_hand_angle(app);
	else
	{
		ft_turn_right_hand(app, 3);
		if (!ft_check_wall_right_hand(app))
			ft_turn_right_hand(app, 1);
		ft_turn_right_hand(app, 3);
		if (!ft_check_wall_right_hand(app))
			ft_turn_right_hand(app, 1);
		ft_move_right_hand(app);
	}
}

void	ft_do_right_hand_check(t_app *app, t_int_vector start_pos)
{
	int		x;

	ft_check_start_pos(app, start_pos, &x);
	while (++x < 4)
	{
		app->right_hand.pos = start_pos;
		if (ft_check_wall_right_hand(app))
			app->right_hand.start_pos = start_pos;
		else
		{
			ft_turn_right_hand(app, 3);
			while (!ft_check_wall_right_hand(app))
			{
				ft_turn_right_hand(app, 1);
				ft_move_right_hand(app);
				ft_turn_right_hand(app, 3);
			}
			app->right_hand.start_pos = app->right_hand.pos;
		}
		ft_right_hand_do_next_move(app);
		while (!(app->right_hand.pos.x == app->right_hand.start_pos.x
				&& app->right_hand.pos.y == app->right_hand.start_pos.y))
			ft_right_hand_do_next_move(app);
		ft_turn_right_hand(app, (char)((x + 1) % 4));
	}
}

void	ft_right_hand_check(t_app *app)
{
	if (app->map.map_grid[(int)(app->map.player_start.pos.y)]
	[(int)(app->map.player_start.pos.x - 1.0)] == '1'
	&& app->map.map_grid[(int)(app->map.player_start.pos.y)]
	[(int)(app->map.player_start.pos.x + 1.0)] == '1'
	&& app->map.map_grid[(int)(app->map.player_start.pos.y - 1.0)]
	[(int)(app->map.player_start.pos.x)] == '1'
	&& app->map.map_grid[(int)(app->map.player_start.pos.y + 1.0)]
	[(int)(app->map.player_start.pos.x)] == '1')
		return ;
	ft_do_right_hand_check(app, (t_int_vector)
	{
		(int)app->map.player_start.pos.x,
		(int)app->map.player_start.pos.y
	});
}
