/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_image.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 02:24:29 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/05 01:07:44 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_init_image(t_app *app, t_img *img)
{
	*img = (t_img) {
		.width = img->width > 0 && img->width < app->win_width
				? img->width : app->win_width,
		.height = img->height > 0 && img->height < app->win_height
				? img->height : app->win_height,
		.coeff = (t_vector) {
			.x = img->coeff.x ? img->coeff.x : 0,
			.y = img->coeff.y ? img->coeff.y : 0
		}
	};
	img->data = mlx_new_image(app->mlx, img->width, img->height);
	img->addr = mlx_get_data_addr(img->data, &img->bits_per_pixel,
			&img->line_length, &img->endian);
}
