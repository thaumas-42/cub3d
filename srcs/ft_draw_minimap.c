/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_minimap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/28 17:53:00 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/16 16:12:43 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		ft_set_bg_pixel(t_app *app)
{
	int		x;
	int		y;
	t_color color_bg;

	y = -1;
	x = -1;
	color_bg = ft_create_argb(255, 100, 100, 100);
	while (++y < app->minimap.height)
	{
		while (++x < app->minimap.width)
			if (y / app->minimap.coeff.y < app->map.map_max_height
			&& x / app->minimap.coeff.x < app->map.map_max_width
			&& app->map.map_grid[(int)(y / app->minimap.coeff.y)]
			[(int)(x / app->minimap.coeff.x)] == '0')
				ft_set_pixel(&app->frame, x, y, color_bg);
		x = -1;
	}
}

t_vector	ft_get_coeff(t_app *app)
{
	return ((t_vector) {
		.x = (double)app->minimap.width / (double)app->map.map_max_width,
		.y = (double)app->minimap.height / (double)app->map.map_max_height
	});
}

void		ft_set_minimap_player(t_app *app, t_vector coeff)
{
	t_color	color;
	int		x;
	int		y;

	y = -6;
	color = ft_create_argb(0, 255, 0, 0);
	while (++y < 6)
	{
		x = (int)(-6 - (y * app->player.plane.x));
		while (++x < (int)(6 + (y * app->player.plane.x)))
			ft_set_pixel(&app->frame, (int)(app->player.pos.x * coeff.x) + x,
					(int)(app->player.pos.y * coeff.y) + y, color);
	}
	x = -1;
	color = ft_create_argb(0, 0, 255, 17);
	while (++x < app->win_width)
		ft_set_pixel(&app->frame, app->ray_hitpos_array[x].x * coeff.x,
				app->ray_hitpos_array[x].y * coeff.y, color);
}

void		ft_set_minimap(t_app *app)
{
	double	coeff_x;
	double	coeff_y;
	double	x;
	double	y;

	coeff_x = app->minimap.coeff.x;
	coeff_y = app->minimap.coeff.y;
	ft_set_bg_pixel(app);
	y = -1;
	while (app->map.map_grid[(int)(++y / coeff_y)])
	{
		x = -1;
		while (app->map.map_grid[(int)(y / coeff_y)][(int)(++x / coeff_x)])
		{
			if (app->map.map_grid[(int)(y / coeff_y)]
			[(int)(x / coeff_x)] == '1')
				ft_set_pixel(&app->frame, (int)x, (int)y,
						ft_create_argb(0, 200, 200, 200));
			else if (app->map.map_grid[(int)(y / coeff_y)]
			[(int)(x / coeff_x)] == '2')
				ft_set_pixel(&app->frame, (int)x, (int)y,
						ft_create_argb(0, 0, 0, 200));
		}
	}
	ft_set_minimap_player(app, app->minimap.coeff);
}

void		ft_draw_minimap(t_app *app)
{
	if (!app->player.show)
		return ;
	else if (app->player.show == 2)
	{
		app->minimap.width = app->win_width - 1;
		app->minimap.height = app->win_height - 1;
		app->minimap.coeff = ft_get_coeff(app);
	}
	else
	{
		app->minimap.width = (MINIMAP_SIZE < app->win_width - 1) ? MINIMAP_SIZE
				: app->win_width - 1;
		app->minimap.height = (MINIMAP_SIZE < app->win_height - 1) ?
				MINIMAP_SIZE : app->win_height - 1;
		app->minimap.coeff = ft_get_coeff(app);
	}
	ft_set_minimap(app);
}
