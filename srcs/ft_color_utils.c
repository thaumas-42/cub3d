/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 02:10:01 by tbrouill          #+#    #+#             */
/*   Updated: 2020/04/20 14:51:53 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_color	ft_create_argb(t_color a, t_color r, t_color g, t_color b)
{
	return (a << 24 | r << 16 | g << 8 | b);
}

t_color	get_b(t_color argb)
{
	return (argb & 0xFF);
}

t_color	get_g(t_color argb)
{
	return (argb & 0xFF00);
}

t_color	get_r(t_color argb)
{
	return (argb & 0xFF0000);
}

t_color	get_a(t_color argb)
{
	return (argb & 0xFF000000);
}
