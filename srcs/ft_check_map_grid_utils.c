/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_map_grid_utils.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/28 16:20:20 by thaumas           #+#    #+#             */
/*   Updated: 2020/07/16 11:13:42 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_check_multi_player(t_app *app, int x, int y)
{
	if (!ft_check_map_indexes(app, x, y))
		return ;
	if (app->map.player_start.pos.x && app->map.player_start.pos.y
	&& (app->map.map_grid[y][x] == 'S' || app->map.map_grid[y][x] == 'W'
	|| app->map.map_grid[y][x] == 'E' || app->map.map_grid[y][x] == 'N'))
		ft_init_error(app, ERROR_MULTI_PLAYER);
}

void	ft_check_map_grid_validation(t_app *app, int x, int y)
{
	if (!ft_check_map_indexes(app, x, y))
		return ;
	if (app->map.map_grid[y][x] == 'N' || app->map.map_grid[y][x] == 'S'
		|| app->map.map_grid[y][x] == 'E' || app->map.map_grid[y][x] == 'W')
		app->map.map_grid[y][x] = '0';
	else if (!(app->map.map_grid[y][x] == '1' || app->map.map_grid[y][x] == '0'
			|| app->map.map_grid[y][x] == '2' || app->map.map_grid[y][x] == ' '
			|| app->map.map_grid[y][x] == '\t'))
		ft_init_error(app, ERROR_MAP_BAD_CHAR);
}
