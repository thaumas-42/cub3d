/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 02:54:05 by tbrouill          #+#    #+#             */
/*   Updated: 2020/05/13 15:31:00 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		ft_get_win_size(t_app *app, const char *str)
{
	char	**tmp;
	int		max_x;
	int		max_y;
	int		res;

	app->map.is_parsed.res ? ft_init_error(app, ERROR_M_R) :
	(app->map.is_parsed.res = 1);
	mlx_get_screen_size(app->mlx, &max_x, &max_y);
	tmp = ft_split(str, ' ');
	if (tmp[1][0] == '\0')
		ft_init_error(app, ERROR_R);
	if (tmp[1])
		app->win_width =
				(res = abs(ft_atoi(tmp[1]))) > max_x ? max_x : res;
	if (tmp[2])
		app->win_height =
				(res = abs(ft_atoi(tmp[2]))) > max_y ? max_y : res;
	if (tmp[2])
		free(tmp[2]);
	if (tmp[1])
		free(tmp[1]);
	free(tmp[0]);
	free(tmp);
	if (!app->win_width || !app->win_height)
		ft_init_error(app, ERROR_R);
}

void		ft_get_floor_color(t_app *app, const char *str)
{
	char	**tmp;
	int		r;
	int		g;
	int		b;

	if (app->map.is_parsed.floor)
		ft_init_error(app, ERROR_M_F);
	app->map.is_parsed.floor = 1;
	tmp = ft_split(str + 2, ',');
	if (ft_str_array_len(tmp) != 3)
		ft_init_error(app, ERROR_F);
	r = ft_atoi2(app, tmp[0]);
	g = ft_atoi2(app, tmp[1]);
	b = ft_atoi2(app, tmp[2]);
	if (r <= -1 || g <= -1 || b <= -1 || r > 255 || g > 255 || b > 255)
		ft_init_error(app, ERROR_F);
	app->map.f_color = ft_create_argb(0, r, g, b);
	free(tmp[0]);
	free(tmp[1]);
	free(tmp[2]);
	free(tmp);
}

void		ft_get_roof_color(t_app *app, const char *str)
{
	char	**tmp;
	int		r;
	int		g;
	int		b;

	if (app->map.is_parsed.ceil)
		ft_init_error(app, ERROR_M_C);
	app->map.is_parsed.ceil = 1;
	tmp = ft_split(str + 2, ',');
	if (ft_str_array_len(tmp) != 3)
		ft_init_error(app, ERROR_C);
	r = ft_atoi2(app, tmp[0]);
	g = ft_atoi2(app, tmp[1]);
	b = ft_atoi2(app, tmp[2]);
	if (r <= -1 || g <= -1 || b <= -1 || r > 255 || g > 255 || b > 255)
		ft_init_error(app, ERROR_C);
	app->map.c_color = ft_create_argb(0, r, g, b);
	free(tmp[0]);
	free(tmp[1]);
	free(tmp[2]);
	free(tmp);
}

static void	ft_parse_line_from_file(t_app *app, char **str, int map_fd)
{
	if (**str == 'R' && (*str)[1] == ' ')
		ft_get_win_size(app, *str);
	else if (**str == 'N' && (*str)[1] == 'O' && (*str)[2] == ' '
	&& !ft_check_multi_iteration(app, 'N'))
		get_texture(app, *str, &app->map.n_texture, ERROR_NO);
	else if (**str == 'S' && (*str)[1] == 'O' && (*str)[2] == ' '
	&& !ft_check_multi_iteration(app, 'S'))
		get_texture(app, *str, &app->map.s_texture, ERROR_SO);
	else if (**str == 'W' && (*str)[1] == 'E' && (*str)[2] == ' '
	&& !ft_check_multi_iteration(app, 'W'))
		get_texture(app, *str, &app->map.w_texture, ERROR_WE);
	else if (**str == 'E' && (*str)[1] == 'A' && (*str)[2] == ' '
	&& !ft_check_multi_iteration(app, 'E'))
		get_texture(app, *str, &app->map.e_texture, ERROR_EA);
	else if (**str == 'S' && (*str)[1] == ' '
	&& !ft_check_multi_iteration(app, 's'))
		get_texture(app, *str, &app->map.sprite_texture, ERROR_S);
	else if (**str == 'F' && (*str)[1] == ' ')
		ft_get_floor_color(app, *str);
	else if (**str == 'C' && (*str)[1] == ' ')
		ft_get_roof_color(app, *str);
	else if (**str)
		ft_get_map_grid(app, str, map_fd);
	if (*str)
		free(*str);
}

void		ft_parse_from_file(t_app *app, int map_fd)
{
	char	*str;

	while (get_next_line(map_fd, &str) > 0)
		ft_parse_line_from_file(app, &str, map_fd);
	ft_parse_line_from_file(app, &str, map_fd);
}
