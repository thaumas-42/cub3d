/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_camera.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 23:44:33 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/15 16:26:03 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_set_pixel_line(t_app *app, t_ray *ray, t_uint x)
{
	t_color		color;
	t_vector	img_pos;
	int			line_start;
	int			line_end;
	int			y;

	y = -1;
	ft_set_line_init(app, ray, &line_start, &line_end);
	ft_set_line_init_img_pos(app, ray, &img_pos, line_start);
	while (++y < app->win_height)
	{
		if (y >= line_start && y <= line_end)
		{
			img_pos.y = (img_pos.y + ray->texture_step >= ray->texture->height)
					? ray->texture->height : img_pos.y + ray->texture_step;
			color = *ft_get_pixel_addr(ray->texture, (int)img_pos.x,
							(int)img_pos.y);
		}
		else if (y < line_start)
			color = app->map.c_color;
		else
			color = app->map.f_color;
		ft_set_pixel(&app->frame, x, y, color);
	}
	ft_set_line_set_hitpos_array(app, ray);
}

void	ft_ray_init(t_app *app, t_vector *side_dist, t_ray *ray)
{
	app->player.camera.x = 2 * ray->x / (double)app->win_width - 1;
	*ray = (t_ray) {
			.x = ray->x,
			.pos.x = app->player.pos.x, .pos.y = app->player.pos.y,
			.dir.x = app->player.dir.x + app->player.plane.x
					* app->player.camera.x,
			.dir.y = app->player.dir.y + app->player.plane.y
					* app->player.camera.x
	};
	ray->map = (t_int_vector){(int)ray->pos.x, (int)ray->pos.y};
	if (!ray->dir.x || !ray->dir.y)
		ray->delta_dist = app->last_ray.delta_dist;
	else
		ray->delta_dist = (t_vector) {
				.x = (!ray->dir.y) ? 1 : fabs(1 / ray->dir.x),
				.y = (!ray->dir.y) ? 1 : fabs(1 / ray->dir.y)
		};
	ray->step.x = (ray->dir.x < 0) ? -1 : 1;
	side_dist->x = (ray->dir.x < 0) ? (ray->pos.x - ray->map.x)
			* ray->delta_dist.x
			: (ray->map.x + 1.0 - ray->pos.x) * ray->delta_dist.x;
	ray->step.y = (ray->dir.y < 0) ? -1 : 1;
	side_dist->y = (ray->dir.y < 0) ? (ray->pos.y - ray->map.y)
			* ray->delta_dist.y
			: (ray->map.y + 1.0 - ray->pos.y) * ray->delta_dist.y;
}

void	ft_get_camera_frame(t_app *app)
{
	t_vector	side_dist;
	t_ray		ray;

	ray.x = -1;
	while (++ray.x < app->win_width)
	{
		app->last_ray = ray;
		ft_ray_init(app, &side_dist, &ray);
		ft_dda(app, &ray, &side_dist);
		if (ray.hit_side < 2)
			ray.wall_dist = (ray.map.x - ray.pos.x +
					(double)(1 - ray.step.x) / 2) / ray.dir.x;
		else
			ray.wall_dist = (ray.map.y - ray.pos.y +
					(double)(1 - ray.step.y) / 2) / ray.dir.y;
		app->dist_buffer[ray.x] = ray.wall_dist;
		ray.line_height = (int)(app->win_coeff * app->win_height
				/ ray.wall_dist);
		ft_set_pixel_line(app, &ray, ray.x);
	}
}

void	ft_draw_camera(t_app *app)
{
	ft_init_image(app, &app->frame);
	ft_get_camera_frame(app);
}
