/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 05:24:20 by tbrouill          #+#    #+#             */
/*   Updated: 2020/02/21 01:51:22 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		main(int argc, char **argv)
{
	t_app	app;

	app = ft_app_init(argc, argv);
	if (app.save_path)
	{
		ft_frame(&app);
		ft_quit(&app);
	}
	mlx_hook(app.win, 02, (1L << 0), &ft_keystroke_press, &app);
	mlx_hook(app.win, 03, (1L << 1), &ft_keystroke_release, &app);
	mlx_hook(app.win, 17, (1L << 17), &ft_quit, &app);
	mlx_loop_hook(app.mlx, &ft_frame, &app);
	mlx_loop(app.mlx);
	return (0);
}
