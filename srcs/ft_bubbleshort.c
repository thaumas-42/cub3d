/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bubbleshort.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/03 00:36:24 by tbrouill          #+#    #+#             */
/*   Updated: 2020/06/03 17:53:01 by root             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_sprite	*get_element_number(t_sprite *sprite, int i)
{
	int	j;

	j = -1;
	while (sprite->next && ++j < i)
		sprite = sprite->next;
	return (sprite);
}

t_sprite	swap2(t_sprite *sprite1, t_sprite *next)
{
	return ((t_sprite) {
				.v_dist = sprite1->v_dist,
				.pos = sprite1->pos,
				.show = sprite1->show,
				.dist = sprite1->dist,
				.screen_x = sprite1->screen_x,
				.img = sprite1->img,
				.next = next
		});
}

void		swap(t_sprite *sprite1, t_sprite *sprite2)
{
	t_sprite	tmp;

	tmp = swap2(sprite1, NULL);
	*sprite1 = swap2(sprite2, sprite1->next);
	*sprite2 = swap2(&tmp, sprite2->next);
}

void		ft_bubbleshort(t_sprite *sprite_list, int len)
{
	int			i;
	int			j;
	t_sprite	*sprite_j;

	i = -1;
	len--;
	while (++i < len)
	{
		j = -1;
		while (++j < len - i)
		{
			sprite_j = get_element_number(sprite_list, j);
			if ((sprite_j->dist < sprite_j->next->dist))
				swap(sprite_j, sprite_j->next);
		}
	}
}
