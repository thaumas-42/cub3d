/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_app_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/27 02:37:09 by tbrouill          #+#    #+#             */
/*   Updated: 2020/05/14 20:33:28 by root             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_app	ft_app_init(int argc, char **argv)
{
	t_app	app;
	char	*str;

	app = (t_app) {
			.save_path = ft_get_save_path(argc, argv),
			.map = (t_map) {.f_color = -1, .c_color = -1},
			.minimap = (t_img) {.width = MINIMAP_SIZE}
	};
	if (argc < 2)
		ft_init_error(&app, ERROR_NO_CFG);
	if (app.save_path && app.save_path[0] == '\a')
		ft_init_error(&app, ERROR_INV_ARG);
	app.mlx = mlx_init();
	if (!app.mlx)
		ft_init_error(&app, ERROR_MLX);
	ft_parser(argv[1], &app);
	str = ft_strjoin("CUB3D\t", argv[1]);
	if (!app.save_path)
		app.win = mlx_new_window(app.mlx, app.win_width, app.win_height, str);
	free(str);
	app.win_coeff = ((double)app.win_width / app.win_height) / 2;
	ft_malloc_string(&app, (char **)&app.dist_buffer,
			(int)sizeof(double) * app.win_width + 1);
	return (app);
}
