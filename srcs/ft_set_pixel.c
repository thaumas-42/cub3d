/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_pixel.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/29 02:46:56 by tbrouill          #+#    #+#             */
/*   Updated: 2020/02/14 01:23:04 by tbrouill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_color	*ft_get_pixel_addr(t_img *img, t_uint x, t_uint y)
{
	char	*pixel;

	pixel = img->addr + (y * img->line_length + x * (img->bits_per_pixel / 8));
	return ((t_color *)pixel);
}

void	ft_set_pixel(t_img *img, t_uint x, t_uint y, t_color color)
{
	t_uint	*pixel;

	pixel = ft_get_pixel_addr(img, x, y);
	*pixel = color;
}
