/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_quit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 00:09:48 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/12 16:04:28 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int	put_error(t_app *app)
{
	int	error_no;

	error_no = 0;
	if (app->error)
	{
		ft_putstr_fd("Error\n", 1);
		ft_putendl_fd(app->error, 1);
		free(app->error);
		error_no = 1;
	}
	return (error_no);
}

int	ft_quit(t_app *app)
{
	int			i;
	int			error_no;
	t_sprite	*sprite;

	i = -1;
	error_no = 0;
	if (app->mlx && app->win)
		mlx_destroy_window(app->mlx, app->win);
	if (app->save_path)
		free(app->save_path);
	if (app->map.map_grid)
		while (app->map.map_grid[++i])
			free(app->map.map_grid[i]);
	if (app->dist_buffer)
		free(app->dist_buffer);
	while (app->map.sprite_list)
	{
		sprite = app->map.sprite_list;
		app->map.sprite_list = app->map.sprite_list->next;
		free(sprite);
	}
	error_no = put_error(app);
	exit(error_no);
}
