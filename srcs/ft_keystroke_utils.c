/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keystroke_utils.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: root <root@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/14 18:47:01 by root              #+#    #+#             */
/*   Updated: 2020/07/16 12:38:06 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_keystroke_do_turn_right(t_app *app)
{
	if (app->key_mem.right)
	{
		app->player.dir.x = app->player.dir.x * cos(ROT_SPEED)
				- app->player.dir.y * sin(ROT_SPEED);
		app->player.dir.y = app->player_old.dir.x * sin(ROT_SPEED)
				+ app->player.dir.y * cos(ROT_SPEED);
		app->player.plane.x = app->player.plane.x * cos(ROT_SPEED)
				- app->player.plane.y * sin(ROT_SPEED);
		app->player.plane.y = app->player_old.plane.x * sin(ROT_SPEED)
				+ app->player.plane.y * cos(ROT_SPEED);
	}
}

void	ft_keystroke_do_turn_left(t_app *app)
{
	if (app->key_mem.left)
	{
		app->player.dir.x = app->player.dir.x * cos(-ROT_SPEED)
				- app->player.dir.y * sin(-ROT_SPEED);
		app->player.dir.y = app->player_old.dir.x * sin(-ROT_SPEED)
				+ app->player.dir.y * cos(-ROT_SPEED);
		app->player.plane.x = app->player.plane.x * cos(-ROT_SPEED)
				- app->player.plane.y * sin(-ROT_SPEED);
		app->player.plane.y = app->player_old.plane.x * sin(-ROT_SPEED)
				+ app->player.plane.y * cos(-ROT_SPEED);
	}
}

void	ft_keystroke_do_move_right(t_app *app)
{
	if (app->key_mem.mv_right)
	{
		if (app->map.map_grid[(int)app->player.pos.y]
		[(int)(app->player.pos.x - SPEED * app->player.dir.y)] != '1')
			app->player.pos.x -= SPEED * app->player.dir.y;
		if (app->map.map_grid[(int)(app->player.pos.y
		- SPEED * -app->player.dir.x)][(int)app->player.pos.x] != '1')
			app->player.pos.y -= SPEED * -app->player.dir.x;
	}
}

void	ft_keystroke_do_move_left(t_app *app)
{
	if (app->key_mem.mv_left)
	{
		if (app->map.map_grid[(int)app->player.pos.y]
		[(int)(app->player.pos.x - SPEED * -app->player.dir.y)] != '1')
			app->player.pos.x -= SPEED * -app->player.dir.y;
		if (app->map.map_grid[(int)(app->player.pos.y
		- SPEED * app->player.dir.x)][(int)app->player.pos.x] != '1')
			app->player.pos.y -= SPEED * app->player.dir.x;
	}
}

void	ft_keystroke_do_move_down(t_app *app)
{
	if (app->key_mem.mv_down)
	{
		if (app->map.map_grid[(int)app->player.pos.y]
		[(int)(app->player.pos.x - SPEED * app->player.dir.x)] != '1')
			app->player.pos.x -= SPEED * app->player.dir.x;
		if (app->map.map_grid[(int)(app->player.pos.y
		- SPEED * app->player.dir.y)][(int)app->player.pos.x] != '1')
			app->player.pos.y -= SPEED * app->player.dir.y;
	}
}
