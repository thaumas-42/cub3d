/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/26 23:14:55 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/14 16:41:13 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	ft_parser(char *filepath, t_app *app)
{
	int		map_fd;

	app->map.init = 1;
	if (!ft_strnstr(filepath, ".cub", ft_strlen(filepath))
	|| (map_fd = open(filepath, O_RDONLY)) == -1)
		ft_init_error(app, ERROR_NO_FILE);
	else
		ft_parse_from_file(app, map_fd);
	if (!app->map.is_parsed.map || !app->map.is_parsed.floor
	|| !app->map.is_parsed.ceil || !app->map.is_parsed.north
	|| !app->map.is_parsed.south || !app->map.is_parsed.east
	|| !app->map.is_parsed.west || !app->map.is_parsed.sprite
	|| !app->map.is_parsed.res)
		ft_init_error(app, ERROR_NO_PARAM);
	app->map.player_start.plane = (t_vector){
			app->map.player_start.dir.x * cos(M_PI_2)
			- app->map.player_start.dir.y * sin(M_PI_2),
			app->map.player_start.dir.x * sin(M_PI_2)
			+ app->map.player_start.dir.y * cos(M_PI_2)};
	app->player = app->map.player_start;
	app->player.pos = (t_vector)
			{app->player.pos.x + 0.5, app->player.pos.y + 0.5};
	app->minimap.height = app->map.map_max_height * ft_get_coeff(app).x;
	ft_malloc_string(app, (char **)&app->ray_hitpos_array, sizeof(t_int_vector)
					* app->win_width);
}
