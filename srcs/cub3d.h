/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 08:06:48 by tbrouill          #+#    #+#             */
/*   Updated: 2020/07/17 18:37:55 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_CUB3D_H
# define CUB3D_CUB3D_H
# define LINUX_AZERTY_WASD 42
# define LINUX_AZERTY_ZQSD 84
# define MAC_QWERTY 21
# define KEYBOARD LINUX_AZERTY_WASD
# if KEYBOARD == LINUX_AZERTY_WASD
#  define QUIT 65307
#  define MOVE_FORWARD 119
#  define MOVE_BACKWARD 115
#  define MOVE_LEFT 97
#  define MOVE_RIGHT 100
#  define TURN_LEFT 65361
#  define TURN_RIGHT 65363
#  define TOGGLE_MINIMAP 109
# else
#  if KEYBOARD == LINUX_AZERTY_ZQSD
#   define QUIT 65307
#   define MOVE_FORWARD 122
#   define MOVE_BACKWARD 115
#   define MOVE_LEFT 113
#   define MOVE_RIGHT 100
#   define TURN_LEFT 65361
#   define TURN_RIGHT 65363
#   define TOGGLE_MINIMAP 109
#  else
#   define QUIT 53
#   define MOVE_FORWARD 13
#   define MOVE_BACKWARD 1
#   define MOVE_LEFT 0
#   define MOVE_RIGHT 2
#   define TURN_LEFT 123
#   define TURN_RIGHT 124
#   define TOGGLE_MINIMAP 109
#  endif
# endif
# define ROT_SPEED 0.015
# define SPEED 0.05
# define MINIMAP_SIZE 400
# define ERROR_S "Couldn't open the sprite (S) Texture."
# define ERROR_NO "Couldn't open the North Wall (NO) Texture."
# define ERROR_EA "Couldn't open the East Wall (EA) Texture."
# define ERROR_WE "Couldn't open the West Wall (WE) Texture."
# define ERROR_SO "Couldn't open the South Wall (SO) Texture."
# define ERROR_R "Couldn't parse the resolution (R) parameter."
# define ERROR_F "Couldn't parse the floor color (F) parameter."
# define ERROR_C "Couldn't parse the roof color (C) parameter."
# define ERROR_M_R "Multiple occurrences of resolution (R) parameter."
# define ERROR_M_NO "Multiple occurrences of the North Wall (NO) Texture."
# define ERROR_M_SO "Multiple occurrences of the South Wall (SO) Texture."
# define ERROR_M_EA "Multiple occurrences of the East Wall (EA) Texture."
# define ERROR_M_WE "Multiple occurrences of the West Wall (WE) Texture."
# define ERROR_M_S "Multiple occurrences of the sprite (S) Texture."
# define ERROR_M_F "Multiple occurrences of floor's color (F) parameter."
# define ERROR_M_C "Multiple occurrences of roof's color (C) parameter."
# define ERROR_M_M "Multiple occurrences of the map."
# define ERROR_INV_ARG "Invalid argument."
# define ERROR_NO_CFG "No config file specified."
# define ERROR_NO_PARAM "Missing parameter."
# define ERROR_NO_FILE "Couldn't open the .cub file"
# define ERROR_NO_PLAYER "Player start not found."
# define ERROR_MULTI_PLAYER "Multiple Player starts found."
# define ERROR_MAP_BAD_CHAR "Non-valid character found in the map."
# define ERROR_MAP_OPEN "Map not Closed."
# define ERROR_MLX "Mlx initialisation error."
# define ERROR_MEM "Memory allocation error."
# define NORTH 0
# define EAST 1
# define SOUTH 2
# define WEST 3
# include "../minilibx-linux/mlx.h"
# include <stdlib.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdio.h>
# include <string.h>
# include <math.h>
# include "../libft/get_next_line.h"
# include "../libft/libft.h"

typedef u_int32_t	t_uint;

typedef u_int32_t	t_color;

typedef char		t_dir;

typedef struct		s_key_mem {
	char	left;
	char	right;
	char	mv_up;
	char	mv_down;
	char	mv_left;
	char	mv_right;
}					t_key_mem;

typedef struct		s_vector {
	double	x;
	double	y;
}					t_vector;

typedef struct		s_int_vector {
	int	x;
	int	y;
}					t_int_vector;

typedef struct		s_parsed_mem {
	char			res;
	char			north;
	char			south;
	char			east;
	char			west;
	char			sprite;
	char			ceil;
	char			floor;
	char			map;
}					t_parsed_mem;

typedef struct		s_img {
	void			*data;
	char			*addr;
	int				bits_per_pixel;
	int				endian;
	int				height;
	int				line_length;
	int				width;
	t_vector		coeff;
	t_int_vector	draw_start;
	t_int_vector	draw_end;
}					t_img;

typedef struct		s_player {
	t_vector	pos;
	t_vector	dir;
	t_vector	plane;
	t_vector	camera;
	int			show;
}					t_player;

typedef struct		s_sprite {
	t_vector		v_dist;
	t_vector		pos;
	char			show;
	double			dist;
	int				screen_x;
	t_img			img;
	struct s_sprite	*next;
}					t_sprite;

# pragma pack(1)

typedef struct		s_bmp_header {
	u_int16_t	magic;
	u_int32_t	size;
	u_int32_t	reserved;
	u_int32_t	img_offset;
}					t_bmp_header;

typedef struct		s_dib_header {
	u_int32_t	header_size;
	u_int32_t	img_width;
	u_int32_t	img_height;
	u_int16_t	color_planes_nbr;
	u_int16_t	size_of_pixel;
	u_int32_t	compression_method;
	u_int32_t	image_size;
	u_int32_t	horizontal_res;
	u_int32_t	vertical_res;
	u_int32_t	colors_number;
	u_int32_t	urgent_colors_number;
}					t_dib_header;

# pragma pack(0)

typedef struct		s_ray {
	char			hit;
	char			hit_side;
	double			texture_step;
	double			wall_dist;
	double			wall_pos_x;
	int				line_height;
	int				x;
	t_img			*texture;
	t_int_vector	map;
	t_int_vector	step;
	t_vector		delta_dist;
	t_vector		dir;
	t_vector		pos;
}					t_ray;

typedef struct		s_right_hand {
	t_int_vector	pos;
	t_dir			dir;
	t_int_vector	start_pos;
}					t_right_hand;

typedef struct		s_map {
	int				init;
	int				f_color;
	int				c_color;
	int				map_max_height;
	int				map_max_width;
	char			**map_grid;
	t_img			n_texture;
	t_img			s_texture;
	t_img			w_texture;
	t_img			e_texture;
	t_img			sprite_texture;
	t_player		player_start;
	t_sprite		*sprite_list;
	t_parsed_mem	is_parsed;
	t_vector		*painting_pos;
}					t_map;

typedef struct		s_app {
	char			*error;
	char			*save_path;
	double			*dist_buffer;
	double			win_coeff;
	int				win_height;
	int				win_width;
	t_img			frame;
	t_img			minimap;
	t_map			map;
	t_player		player;
	t_player		player_old;
	void			*mlx;
	void			*win;
	void			*current;
	t_key_mem		key_mem;
	t_right_hand	right_hand;
	t_ray			last_ray;
	t_vector		*ray_hitpos_array;
}					t_app;

char				ft_check_multi_iteration(t_app *app, char param);
char				ft_check_wall_right_hand(t_app *app);
char				*ft_get_save_path(int argc, char **argv);
char				ft_seek_paint(t_vector *painting_list,
						t_int_vector current_pos, t_dir dir);
double				ft_pythagore(t_vector pos1, t_vector pos2);
int					ft_atoi2(t_app *app, const char *str);
int					ft_check_map_indexes(t_app *app, t_uint x, t_uint y);
int					ft_frame(t_app *app);
int					ft_keystroke(t_app *app);
int					ft_keystroke_press(int keycode, t_app *app);
int					ft_keystroke_release(int keycode, t_app *app);
int					ft_quit(t_app *app);
size_t				ft_str_array_len(char **str_array);
t_app				ft_app_init(int argc, char **argv);
t_color				ft_create_argb(t_color a, t_color r, t_color g, t_color b);
t_color				get_b(t_color argb);
t_color				get_g(t_color argb);
t_color				get_r(t_color argb);
t_color				get_a(t_color argb);
t_uint				*ft_get_pixel_addr(t_img *img, t_uint x, t_uint y);
t_vector			ft_get_coeff(t_app *app);
void				ft_bubbleshort(t_sprite *sprite_list, int len);
void				ft_check_map_grid(t_app *app);
void				ft_check_map_grid_validation(t_app *app, int x, int y);
void				ft_check_multi_player(t_app *app, int x, int y);
void				ft_check_start_pos(t_app *app, t_int_vector start_pos,
						int *x);
void				ft_dda(t_app *app, t_ray *ray, t_vector *side_dist);
void				ft_draw_camera(t_app *app);
void				ft_draw_minimap(t_app *app);
void				ft_do_right_hand_check(t_app *app, t_int_vector start_pos);
void				ft_init_error(t_app *app, const char *str);
void				ft_init_image(t_app *app, t_img *img);
void				ft_parse_from_file(t_app *app, int map_fd);
void				ft_get_camera_frame(t_app *app);
void				ft_get_floor_color(t_app *app, const char *str);
void				ft_get_map_grid(t_app *app, char **str, int map_fd);
void				ft_get_roof_color(t_app *app, const char *str);
void				ft_get_win_size(t_app *app, const char *str);
void				ft_keystroke_do_move_up(t_app *app);
void				ft_keystroke_do_move_down(t_app *app);
void				ft_keystroke_do_move_left(t_app *app);
void				ft_keystroke_do_move_right(t_app *app);
void				ft_keystroke_do_turn_left(t_app *app);
void				ft_keystroke_do_turn_right(t_app *app);
void				ft_malloc_string(t_app *app, char **str, int size);
void				ft_move_right_hand(t_app *app);
void				ft_move_right_hand_angle(t_app *app);
void				ft_parser(char *filepath, t_app *app);
void				ft_put_camera_frame(const t_app *app);
void				ft_put_image_to_file(t_img *img, int fd);
void				ft_put_sprite_to_frame(t_app *app, const t_sprite *sprite,
						t_int_vector *sprite_size, t_vector *transform);
void				ft_right_hand_check(t_app *app);
void				ft_set_bg_pixel(t_app *app);
void				ft_set_draw_values(t_app *app, const t_sprite *sprite,
						t_img *img, t_int_vector *sprite_size);
void				ft_set_line_init(t_app *app, t_ray *ray, int *line_start,
						int *line_end);
void				ft_set_line_init_img_pos(t_app *app, t_ray *ray,
						t_vector *img_pos, int line_start);
void				ft_set_line_set_hitpos_array(t_app *app, const t_ray *ray);
void				ft_set_minimap(t_app *app);
void				ft_set_next_pos(t_vector *painting_pos,
						t_int_vector current_pos, char sep);
void				ft_set_paint_east_wall(t_app *app, t_int_vector *pos);
void				ft_set_paint_north_wall(t_app *app, t_int_vector *pos);
void				ft_set_paint_south_wall(t_app *app, t_int_vector *pos);
void				ft_set_paint_west_wall(t_app *app, t_int_vector *pos);
void				ft_set_pixel(t_img *img, t_uint x, t_uint y, t_color color);
void				ft_turn_right_hand(t_app *app, char x);
void				get_texture(t_app *app, char *str, t_img *map_texture,
								char *err);
#endif
