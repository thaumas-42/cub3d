# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tbrouill <tbrouill@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/05/14 20:24:18 by tbrouill          #+#    #+#              #
#    Updated: 2020/07/05 01:42:25 by thaumas          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC = $(SRC_PATH)/ft_check_map_grid_utils.c \
      $(SRC_PATH)/ft_check_map_grid.c \
      $(SRC_PATH)/ft_check_multi_iteration.c \
      $(SRC_PATH)/ft_init_error.c \
      $(SRC_PATH)/ft_put_images.c \
      $(SRC_PATH)/ft_parser_utils.c \
      $(SRC_PATH)/ft_keystroke.c \
      $(SRC_PATH)/ft_get_texture.c \
      $(SRC_PATH)/ft_app_init.c \
      $(SRC_PATH)/ft_keystroke_utils.c \
      $(SRC_PATH)/ft_quit.c \
      $(SRC_PATH)/ft_set_pixel_line_utils.c \
      $(SRC_PATH)/ft_right_hand_utils.c \
      $(SRC_PATH)/ft_draw_camera.c \
      $(SRC_PATH)/ft_right_hand.c \
      $(SRC_PATH)/ft_draw_camera_utils.c \
      $(SRC_PATH)/ft_color_utils.c\
      $(SRC_PATH)/ft_frame.c \
      $(SRC_PATH)/ft_get_map_grid.c \
      $(SRC_PATH)/ft_init_image.c \
      $(SRC_PATH)/ft_set_pixel.c \
      $(SRC_PATH)/ft_draw_minimap.c \
      $(SRC_PATH)/ft_set_paint_on_wall_utils.c \
      $(SRC_PATH)/ft_get_save_path.c \
      $(SRC_PATH)/cub3d.c \
      $(SRC_PATH)/ft_parser.c \
      $(SRC_PATH)/ft_frame_utils.c \
      $(SRC_PATH)/ft_str_array_len.c \
      $(SRC_PATH)/ft_bubbleshort.c

LIBFT_PATH = .

SRC_PATH = ./srcs

LIBFT_BIN = ${LIBFT_PATH}/libft.a

OBJ = $(SRC:.c=.o)

NAME = cub3D

CC = clang

CFLAGS = -Wall -Wextra -Werror

LINK = clang

LINK_FLAGS =  -L. -L./minilibx-linux -lft -lmlx -lm -lX11 -lXext -L/usr/lib -L/usr/X11/lib -L/usr/include/X11 -o $(NAME)

RM = rm -f

.c.o:
			$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)


$(NAME):	libft.a minilibx-linux/libmlx.a $(OBJ)
			${LINK} ${OBJ} ${LINK_FLAGS}

all:		$(NAME)

libft.a:
			cd libft && make all

minilibx-linux/libmlx.a:
			cd minilibx-linux && make all

clean:
			${RM} ${OBJ}
			cd libft && make clean
			cd minilibx-linux && make clean

fclean:		clean
			cd libft && make fclean
			${RM} ${NAME}

re:			fclean all

.PHONY:		all fclean clean re

